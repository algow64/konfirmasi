export const isAuthenticated = () => {
  const jwt = localStorage.getItem('JWT');

  if(!jwt) {
    return false;
  }
  return true;
}

export const getJwt = () => {
  return localStorage.getItem('JWT');
}