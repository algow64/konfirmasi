import React, { Component } from 'react';
import { isAuthenticated } from './helper/utils';
import { Grid } from 'semantic-ui-react';
import Login from './views/Login';
import Ntpn from './views/Ntpn';

class App extends Component {
  render() {
    return (
      <div style={{backgroundColor: '#e5e5e5', height: '100vh'}}>
        <Grid
          textAlign="center"
          style={{ height: '100%', marginTop: '0' }}
          verticalAlign="middle"
        >
          <Grid.Row>
            <Grid.Column style={{ maxWidth: 600 }}>
              { isAuthenticated() ? 
                <Ntpn /> : <Login />
              }
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default App;
