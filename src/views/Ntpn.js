import React, { Component } from 'react';
import { 
  Form, 
  Header, 
  Icon,
  TextArea,
  Modal,
  Button 
} from 'semantic-ui-react';
import { Formik } from 'formik';
import { ntpn } from '../actions/ntpn';
import { retrieve } from '../actions/retrieve';

class Ntpn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      modal: false,
      message: ''
    }
  }

  showDownload = () => {
    this.setState({
      loading: false,
      modal: true
    });
  }

  showModal = (message) => {
    this.setState({ 
      loading: true,
      modal: true,
      message: message 
    });
    setTimeout(() => {
      this.showDownload();
    }, 10000);
  }

  closeModal = () => {
    this.setState({ modal: false })
  }

  getFileName = () => {
    return localStorage.getItem('filerekon');
  }

  render() {
    return (
      <div>
        <Header
          as="h1"
          textAlign="center"
        >
          Masukkan kode billing
        </Header>
        <p style={{
          textAlign: 'left'
        }}>
          Pisahkan dengan tanda koma tanpa spasi
        </p>
        <Formik
          enableReinitialize={true}
          initialValues={{ billings: '' }}
          onSubmit={(values, { setSubmiting }) => {
            ntpn(values).then(res => {
              const fileName = res.data.file;
              this.showModal('Proses rekon', fileName);
              localStorage.setItem('filerekon', fileName);
            }).catch(err => {
              localStorage.clear();
              window.location.reload();
              this.showModal('Gagal Rekon');
            });
          }}
        >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit
        }) => (
          <Form>
            <Form.Field>
              <TextArea 
                name='billings'
                type='text'
                value={values.billings}
                onChange={handleChange}
                onBlur={handleBlur}
                placeholder='Contoh: 019063052651715,019070522677761,119121131902137,119121131512151,019124276398619,019125088637414,0235086267211071,023513535486159,019124807558111,023717039356006,019124817933487,223513096921006,019070650709521,019073707112914,223513097050060,119121131522125,019074606241181,019063052457145' 
                style={{ minHeight: 200 }}  
              />
            </Form.Field>
            <Form.Field>
            <Button
              fluid
              size='large'
              type='submit' 
              content='Kirim'
              primary
              onClick={handleSubmit}
            />
            </Form.Field>
          </Form>
        )}
        </Formik>
        <Modal size='small' open={this.state.modal} onClose={this.closeModal} style={{textAlign: 'center'}}>
          <Modal.Header>{ this.state.message }</Modal.Header>
          <Modal.Content>
            {
              this.state.loading ? 
              <div>
                <Button basic loading size='big'>
                  Loading
                </Button>
                <p>Tunggu beberapa saat</p>
              </div>
              :
              <div>
                <Button 
                  size='big'
                  color='blue'
                  icon
                  onClick={() => {
                    retrieve(this.getFileName()).then(fileRekon => {
                      window.open(`${fileRekon.config.url}&download=true`);
                    }).catch(() => {
                      this.showModal('Proses rekon');
                    });
                  }}
                >
                  <Icon name='refresh' />
                </Button>
                <p>Refresh file hasil rekon</p>
              </div>
            }
          </Modal.Content>
          <Modal.Actions>
            <Button 
              negative
              onClick={this.closeModal}  
            >
              close
            </Button>
          </Modal.Actions>
        </Modal>
      </div>
    );
  }
}

export default Ntpn;