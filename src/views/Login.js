import React, { Component } from 'react';
import { 
  Button,
  Form,
  Header,
  Grid,
  Message
} from 'semantic-ui-react';
import { Formik } from 'formik';
import { auth } from '../actions/auth';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: {
        display: false,
        color: '',
        message: ''
      }
    }
  }

  render() {
    return (
      <div style={{backgroundColor: '#e5e5e5'}}>
        <Grid
          textAlign="center"
          style={{ height: '100%', marginTop: '0' }}
          verticalAlign="middle"
        >
          <Grid.Row>
            <Grid.Column style={{ maxWidth: 450 }}>
              <Header
                as="h1"
                textAlign="center"
              >
                Login OmSpan
              </Header>
              <Formik
                enableReinitialize={true}
                initialValues={{username: '', password: '', ta: ''}}
                onSubmit={(values, { setSubmiting }) => {
                  auth(values).then(res => {
                    this.setState({message: {
                      display: true,
                      color: 'green',
                      message: res
                    }});
                  }).catch(err => {
                    this.setState({message: {
                      display: true,
                      color: 'red',
                      message: err.response.data.msg
                    }});
                  });
                }}
              >
              {({
                values,
                handleChange,
                handleBlur,
                handleSubmit
              }) => (
                <Form>
                  <Form.Field>
                    <Form.Input 
                      size='big'
                      icon='user' 
                      iconPosition='left'
                      placeholder='nama pengguna...'
                      name='username' 
                      type='text'
                      value={values.username}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </Form.Field>
                    <Form.Input 
                      size='big'
                      icon='key' 
                      iconPosition='left'
                      placeholder='kata sandi...'
                      name='password' 
                      type='password'
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  <Form.Field >
                  </Form.Field>
                  <Form.Field >
                    <Form.Input 
                      list='ta' 
                      name='ta' 
                      placeholder='tahun anggaran'
                      value={values.ta}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    <datalist id='ta'>
                      <option value='2019' />
                      <option value='2020' />
                    </datalist>
                  </Form.Field>
                  <Form.Field>
                    <Button
                      fluid
                      size='big'
                      type='submit' 
                      content='masuk'
                      primary
                      onClick={handleSubmit}
                    />
                  </Form.Field>
                </Form>
              )}
            </Formik>
            { this.state.message.display ? 
              (<Message attached='bottom' color={ this.state.message.color }>
                {this.state.message.message}
              </Message>) : null
            }
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    )
  }
}

export default Login;