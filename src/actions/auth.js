import axios from 'axios';
import { getJwt } from '../api';

export const auth = formData => {
  return axios({
    url: getJwt,
    method: 'post',
    headers: {
      'Content-Type': 'application/json'
    },
    data: formData
  }).then(res => {
    localStorage.setItem('Y_Y', JSON.stringify(res.data.data));
    localStorage.setItem('JWT', res.data.data.token);
    window.location.reload();
    return res.data.data.username;
  });
}