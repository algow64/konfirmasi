import axios from 'axios';
import { download } from '../api';
import { getJwt } from '../helper/utils';

export const retrieve = fileName => {
  return axios({
    url: `${download}?file=${fileName}`,
    method: 'get',
    headers: {
      'Accept': 'applocation/json',
      'Authorization': `Bearer ${getJwt()}`
    }
  }).then(res => {
    return res;
  });
}