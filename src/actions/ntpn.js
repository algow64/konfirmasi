import axios from 'axios';
import { konfirmasi } from '../api';
import { getJwt } from '../helper/utils';

export const ntpn = formData => {
  formData.userid = JSON.parse(localStorage.getItem('Y_Y')).userid;

  return axios({
    url: konfirmasi,
    method: 'post',
    headers: {
      'Accept': 'applocation/json',
      'Authorization': `Bearer ${getJwt()}`
    },
    data: formData
  }).then(res => {
    return res;
  });
}